<?php

namespace App\Exceptions;


use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;


class Handler extends ExceptionHandler
{
    
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof HttpException) {
            $code = $exception->getStatusCode();
            $message = Response::$statusTexts[$code];
            return $this->errorResponse($message, $code);
        }
        if ($exception instanceof ModelNotFoundException) {
            $model = strtolower(class_basename($exception->getModel()));
            return $this->errorResponse("Does not exist any instance of {$model} with the given id", Response::HTTP_NOT_FOUND);
        }
        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse($exception->getMessage(), Response::HTTP_FORBIDDEN);
        }
        if ($exception instanceof AuthenticationException) {
            return $this->errorResponse($exception->getMessage(), Response::HTTP_UNAUTHORIZED);
        }
        if ($exception instanceof ValidationException) {
            $errors = $exception->validator->errors()->getMessages();
            if ($request->header('X-REQUEST-TYPE') == 'mobile') {
                return  $this->mobileException($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
            } else {
                return $this->errorResponse($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        if (env('APP_DEBUG', false)) {
            return parent::render($request, $exception);
        }
        return $this->errorResponse('Unexpected error. Try later', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Method to frame the error JSON response
     * 
     * @param mixed $message
     * @param string $code
     */
    public function errorResponse($message, $code) {
        $data = array();
        return response ()->json ( array_merge ( [ 
            'error' => true,
            'statusCode' => $code,
            'status' => 'error',
            'message' => $message 
        ], $data ), $code );
    }
    /**
     * Method to send validation response for mobile
     *
     * @param $errors
     * @param $code
     * @return array
     */
    protected function mobileException($errors, $code) {
        return  response()->json([
             'message' => array_shift($errors)[0],
             'error' => true,
             'statusCode' => 422,
         ], $code);
    }
}
