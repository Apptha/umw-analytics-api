<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Cache;
use Contus\User\Models\SettingCategory;
use Contus\User\Models\Setting;
use Contus\User\Repositories\SettingsRepository;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {

        if(env('ENABLE_SSL')) {
            \URL::forceScheme('https');
        }
        Schema::defaultStringLength(191);
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(env('ENABLE_SSL')) {
            $this->app['request']->server->set('HTTPS', true);
        }
    }
}
