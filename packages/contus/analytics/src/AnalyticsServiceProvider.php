<?php

/**
 * Service Provider for Analytics
 *
 * @name AnalyticsServiceProvider
 * @vendor Contus
 * @package Analytics
 * @version 1.0
 * @author Contus<developers@contus.in>
 * @copyright Copyright (C) 2016 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Analytics;

use Illuminate\Support\ServiceProvider;
use Contus\Base\Helpers\StringLiterals;

class AnalyticsServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @vendor Contus
     * @pakage Analytics
     *
     * @return void
     */
    public function boot() {
        $package = 'analytics';
        $this->loadTranslationsFrom ( __DIR__ . DIRECTORY_SEPARATOR . StringLiterals::RESOURCES . DIRECTORY_SEPARATOR . 'lang', $package );
        $this->publishes ( [ __DIR__ . DIRECTORY_SEPARATOR . 'config' => config_path ( 'contus/'.$package ) ], 'analytics_config' );
    }
    
    /**
     * Register the application services.
     *
     * @vendor Contus
     * @pakage Analytics
     *
     * @return void
     */
    public function register() {
        include __DIR__ . '/routes/api.php';
        include __DIR__ . '/routes/web.php';
    }
}
