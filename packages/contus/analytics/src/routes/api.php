<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'analytics/api/v2', 'namespace' => 'Contus\Analytics\Api\Controllers\Frontend'], function () {
    Route::group(['middleware' => [ 'cors', 'updatedversion','api',  'jwt-auth:1', 'api.auth' ]], function () {
        Route::post('videoanalytics', 'VideoAnalyticsController@postVideoAnalytics');
    });
  
});
