<?php

namespace Contus\Analytics\Models;

use Contus\Base\Model;
use Contus\Base\BaseAuthenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends BaseAuthenticatable implements JWTSubject {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    /**
     * Morph class name
     *
     * @var string
     */
    protected $morphClass = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email','phone','acesstype','is_active','device_token','device_type','profile_picture','age', 'country_code', 'iso'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [ 'password' ];
    /**
     * The attribute will used to generate url
     *
     * @var array
     */
    protected $url = ['profile_picture'];

    /**
     * Tthe attributes used for soft delete
     */
    protected $dates = [ 'deleted_at' ];

    /**
     * Constructor method
     * sets hidden for customers
     */
    public function __construct()
    {
        parent::__construct();
        $this->setHiddenCustomer([ 'id','password','access_otp_token','access_token','acesstype','created_at','creator_id','expires_at','facebook_auth_id','facebook_user_id','google_auth_id','google_user_id','is_active','login_type','remember_token','updated_at','updator_id','deleted_at','device_token','device_type','forgot_password','mypreferences','pivot','notify_comment','notification_status','notify_newsletter','notify_reply_comment','notify_videos' ]);
    }
    /**
     * funtion to automate operations while Saving
     */
    public function bootSaving()
    {
        $this->saveImage('profile_picture', 'required|mimes:jpeg,gif,png|image|max:5000');
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
