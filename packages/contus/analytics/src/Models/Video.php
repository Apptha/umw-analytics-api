<?php

/**
 * Video Model for videos table in database
 *
 * @name Video
 * @vendor Contus
 * @package Analytics
 * @version 1.0
 * @author Contus<developers@contus.in>
 * @copyright Copyright (C) 2016 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 */
namespace Contus\Analytics\Models;

use Contus\Base\Model;

class Video extends Model{
    /**
     * The database table used by the model.
     *
     * @vendor Contus
     *
     * @package Video
     * @var string
     */
    protected $table = 'videos';
    /**
     * Morph class name
     *
     * @var string
     */
    protected $morphClass = 'videos';
    protected $primaryKey = 'id';
}
