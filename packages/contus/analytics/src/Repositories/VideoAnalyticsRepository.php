<?php
/**
 * Video Analytics Repository
 *
 * To manage the analytics of the videos
 *
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2019 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 *
 *
 */
namespace Contus\Analytics\Repositories;

use Contus\Base\Repository as BaseRepository;
use Contus\Analytics\Models\WatchHistory;
use Contus\Analytics\Models\Video;
use Contus\Analytics\Models\VideoAnalytic;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class VideoAnalyticsRepository extends BaseRepository {
    public function __construct(WatchHistory $watchHistoryModel, Video $videoModel, VideoAnalytic $videoAnalyticsModel) {
        parent::__construct();
        $this->watchHistoryModel = $watchHistoryModel;
        $this->videosModel = $videoModel;
        $this->videoAnalyticsModel = $videoAnalyticsModel;
    }
    /**
     * Method to store analytics data
     * 
     * @return boolean
     */
    public function storeVideoAnalytics(){
        $result = false;
        $videoslug = $this->request->video_slug;
        if($videoslug){
            $videoBuilder = $this->videosModel->where('is_active', 1)->where($this->getKeySlugorId(), $videoslug)->first();
            $videoID = $videoBuilder->id;
            $videoBuilder->increment('view_count');
            $ip = getIPAddress();
            if (!empty(authUser()->id)) {                
                $this->watchHistoryModel = $this->watchHistoryModel->where ( 'video_id',  $videoID )->where('customer_id',  authUser()->id)->first();
            } else {
               $this->watchHistoryModel = $this->watchHistoryModel->where ( 'video_id',  $videoID )->where('ip_address',  $ip)->first();
            }
            if (is_object($this->watchHistoryModel) && !empty($this->watchHistoryModel->id)) { 
                $this->watchHistoryModel->is_active = 1;
                $this->watchHistoryModel->updated_at = Carbon::now()->toDateTimeString();
                $result = $this->watchHistoryModel->save();
            } else {
                $watchHistory = new WatchHistory();
                $watchHistory->video_id = $videoID;
                $watchHistory->customer_id = (!empty(authUser()->id)) ? authUser()->id : '';
                $watchHistory->ip_address = (!empty(authUser()->id)) ? '' : $ip;
                $watchHistory->is_active = 1;
                $result =  $watchHistory->save();
            }
            $this->addVideoAnalytics($videoBuilder);
            return $result;
        }
    }
    /**
     * Method to record video analytics data 
     * @param $video array
     * 
     * @return boolean
     */
    public function addVideoAnalytics($video){
        $ip = '';
        $videoAnalyticsData = array();
        /** This is call to the helper method to the get the IP address */
        $ip = getIPAddress();
         /** This is call to a method to get the current logged in user country based on the IP */
         /** \Torann\GeoIP\ Pakage is used */
         $getcurrentIPLocation = geoip($ip);
         $getcurrentIPLocationFlag = (isset($getcurrentIPLocation->country))?$getcurrentIPLocation->country:'unknown';
         /** Call to method to get the platform (Web, ios or android) of the request */
         $platform = getPlatform();
         $customerId = (!empty(authUser()->id))?authUser()->id:0;
         $videoAnalyticsData = [
             'video_id'=>$video->id,
             'video_title'=>$video->title,
             'customer_id' => $customerId,
             'country' => $getcurrentIPLocationFlag,
             'platform' => $platform,
         ];
         /** Set validator to check if all the parameters exist needed for video analytics */
        $validator = Validator::make($videoAnalyticsData, [
            'video_id' => 'required|integer',
            'video_title' => 'required|string',
            'customer_id' => 'required|integer',
            'country' => 'required|string',
            'platform' => 'required|string',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages()->toArray();
            foreach($messages as $message){
                app('log')->error(' ###File : VideoTrait.php ##Message : The video analytics insertion failed  ' .' #Error : ' . $message[0]);
            }
       }else{
        $videoAnalytic = $this->videoAnalyticsModel;
            try{
                $videoAnalytic->fill($videoAnalyticsData);
                return ($videoAnalytic->save())?true:false;
            }
            catch(Exception $e) {
                app('log')->error(' ###File : VideoTrait.php ##Message : The video analytics insertion failed  ' .' #Error : ' . $e->getMessage());
            }
        }
        return false;
    }
}