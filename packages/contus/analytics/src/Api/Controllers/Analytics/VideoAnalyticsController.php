<?php
/**
 * Video Analytics Controller
 *
 * To manage the analytics of the videos
 *
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 * @license GNU General Public License http://www.gnu.org/copyleft/gpl.html
 *
 *
 */
namespace Contus\Analytics\Api\Controllers\Frontend;

use Contus\Base\ApiController;
use Contus\Analytics\Repositories\VideoAnalyticsRepository;

class VideoAnalyticsController extends ApiController {
    public function __construct(VideoAnalyticsRepository $VideoAnalyticsRepository) {
        parent::__construct();
        $this->repository = $VideoAnalyticsRepository;
    }
    /**
     * Method to store the video analytics
     * 
     * @return Illuminate\Http\JsonResponse
     */
    public function postVideoAnalytics() {
        $response = $this->repository->storeVideoAnalytics();
        return ($response) ? $this->getSuccessJsonResponse([], trans('analytics::analytics.success'))
        : $this->getErrorJsonResponse([], trans('analytics::analytics.error'));
    }
}